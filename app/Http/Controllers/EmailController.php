<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function send_email()
    {
        $data = array('name' => "Nikita Singh");
        $result =  Mail::send('mail', $data, function ($message) {
            $message->to('nikitas.dollop@gmail.com', 'Tutorials Point')->subject('Laravel Basic Testing Mail');
            $message->from('nikitasingh202022@gmail.com', 'Nikita Singh');
            // dd($message);
        });
        return response()->json(['message' => 'email uploaded successfully']);
    }
}
